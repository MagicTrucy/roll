#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>
#include <stdlib.h>
#include <stdbool.h>

int check_validity (char* throw)
{
  char* regex = "^[0-9]+d[0-9]+$";
  regex_t* preg = malloc(sizeof (regex_t));
  int regerrorcode = regcomp(preg, regex, REG_ICASE | REG_NOSUB | REG_EXTENDED);
  if (regerrorcode != 0)
    regerror(regerrorcode, preg, regex, strlen(regex));
  int result = regexec(preg, throw, 0, NULL, 0);
  regfree(preg);
  free(preg);
  return result;
}

size_t where_is_the_d (char* throw)
{
  for (size_t i = 0; i < strlen(throw); i++)
    if (throw[i] == 'd' || throw[i] == 'D')
      return i;
  return 0;
}
#if 0
void parse(char* throw)
{
  for (size_t i = 0; i < strlen(throw); i++)
  {
    if (throw[i] == 'd' || throw [i] == 'D')

  }
}
#endif
int main(int argc, char** argv)
{
  if (argc >= 2)
    check_validity(argv[1]);
  else
    return 1;
  return 0;
}
