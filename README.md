# ROLL #

Throws some dice.

## Usage ##

./roll NdM throws N M-sided dice.

## How do I compile? ##

gcc -std=c11 -Wall